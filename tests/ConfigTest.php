<?php

namespace Ridrog\AdminBase\Test;

use Illuminate\Support\Facades\Artisan;
use Ridrog\AdminBase\Test\TestCase as TestCase;

class ConfigTest extends TestCase
{

    /** @test */
    public function the_config_exists()
    {
        $path = __DIR__.'/../config/admin_base.php';

        $this->assertFileExists($path);
    }

    /** @test */
    public function publishing_config_works()
    {
        Artisan::call( 'vendor:publish', ['--tag' => 'admin_base-config']);

        $this->assertFileExists(config_path('admin_base.php'));
    }

}