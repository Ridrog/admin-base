<?php

namespace Ridrog\AdminBase\Test;

use Ridrog\AdminBase\AdminBase;
use Ridrog\AdminBase\Facades\AdminBaseFacade;
use Illuminate\Support\ServiceProvider;
use Ridrog\AdminBase\AdminBaseServiceProvider;
use Ridrog\AdminBase\Test\TestCase as TestCase;

class FacadeTest extends TestCase
{

    /**
     *
     * @var AdminBaseServiceProvider
     */
    private $provider;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->provider = $this->app->getProvider(AdminBaseServiceProvider::class);

    }

    /**
     * Tear Down
     */
    public function tearDown()
    {
        unset($this->provider);

        parent::tearDown();
    }

    /** @test */
    public function test_the_facade()
    {
        $hello = AdminBaseFacade::hello();

        $this->assertTrue($hello === "hello");
    }

    /** @test */
    public function test_the_class()
    {
        $admin_base = new AdminBase();

        $this->assertTrue($admin_base->hello() === "hello");
    }

    /** @test */
    public function test_the_helper()
    {
        $admin_base = admin_base();

        $this->assertTrue($admin_base->hello() === "hello");
    }
}