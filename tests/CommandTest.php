<?php

namespace Ridrog\AdminBase\Test;

use Illuminate\Support\Facades\Artisan;
use Ridrog\AdminBase\AdminBaseServiceProvider;
use Ridrog\AdminBase\Test\TestCase as TestCase;

class CommandTest extends TestCase
{
    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_run_the_admin_base_test_command()
    {
        Artisan::call('admin_base:test');

        $this->assertTrue(true);
    }

}