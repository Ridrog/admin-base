<?php

namespace Ridrog\AdminBase\Test;

use Ridrog\AdminBase\AdminBase;
use Illuminate\Support\ServiceProvider;
use Ridrog\AdminBase\AdminBaseServiceProvider;
use Ridrog\AdminBase\Test\TestCase as TestCase;

class HelperTest extends TestCase
{

    /**
     *
     * @var AdminBaseServiceProvider
     */
    private $provider;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->provider = $this->app->getProvider(AdminBaseServiceProvider::class);

    }

    /**
     * Tear Down
     */
    public function tearDown()
    {
        unset($this->provider);

        parent::tearDown();
    }

    /** @test */
    public function it_returns_hello()
    {
        $admin_base = admin_base();

        $this->assertTrue($admin_base->hello() === "hello");
    }

}