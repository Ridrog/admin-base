<?php

namespace Ridrog\AdminBase\Facades;

use Illuminate\Support\Facades\Facade;

class AdminBaseFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'admin_base';
    }
}