<?php

namespace Ridrog\AdminBase\Listeners;

use Illuminate\Support\Facades\Log;
use Ridrog\AdminBase\Events\AdminBaseEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminBaseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminBaseEvent  $event
     * @return void
     */
    public function handle(AdminBaseEvent $event)
    {
        Log::info("admin_base event was fired");
    }
}
