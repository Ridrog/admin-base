<?php

Route::get('testroute', function(){
    return "The Routes are loaded";
});

Route::get('testtrans', function(){
    return trans('admin_base::example.message');
});

Route::group(['middleware' => 'web', 'namespace' => 'Ridrog\AdminBase\Http\Controllers'], function () {
    Route::get('/controllertest', 'AdminBaseController@test')->name('test-controller');
});
