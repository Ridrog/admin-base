<?php

namespace Ridrog\AdminBase\Http\Controllers;

use Illuminate\Http\Request;

class AdminBaseController extends Controller
{
    public function test()
    {
        return "The Controller worked";
    }
}
