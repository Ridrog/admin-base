[Home](index.md) | [Installation](install.md) | [Usage](usage.md) | [Config](config.md)  

# Installation

## Require it

```
composer require ridrog/admin_base
```

## Include Service Provider 

```
'providers' => [
    ...
    Ridrog\AdminBase\AdminBaseServiceProvider::class,
    ...
 ],
```

Register the Facade

```
'aliases' => [
    ...
    'AdminBase' => Ridrog\AdminBase\Facades\AdminBaseFacade::class,
    ...
];
```



