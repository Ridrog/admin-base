[back](../index.md)  

# Translations
 
 **Publish Translations**
 ```
 php artisan vendor:publish --tag=admin_base-translations
 ```